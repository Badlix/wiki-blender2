import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'home_page.dart';

class basicDviewportPage extends StatelessWidget {
  static const routeName = 'basic_3D_viewport';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Basics - 3D viewport"),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.home),
              onPressed: () {
                Navigator.of(context).pushNamedAndRemoveUntil(HomePage.routeName, (Route<dynamic> routeName) => false);
              },
            )
          ],
        ),
        body: ListView(
          children: <Widget>[
            ExpansionTile(title: Text('Where is 3D viewport ?'), children: <Widget>[
              Column(
                children: <Widget>[
                  Image.asset(
                    'assets/images/3D_viewport.png',
                  )
                ],
              ),
            ]),
            Row(
              children: <Widget>[
                Card(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                  elevation: 5,
                  child: Column(
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              'VIEW NAVIGATION',
                              style: TextStyle(fontSize: 30),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(bottom: 8, left: 8, right: 8),
                            child: Text(
                              "Faire tourner le point de vue -> MMB\nDeplacer lateralement le point de vue -> Shift + MMB\nRecentrer le point de vur sur un objet -> . suppr",
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                )
              ],
            ),
            Row(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(bottom: 20, top: 20),
                  child: Text('BASIC TOOL', style: TextStyle(fontSize: 30)),
                )
              ],
            ),
            Table(
                columnWidths: {0: FlexColumnWidth(2), 1: FlexColumnWidth(7), 2: FlexColumnWidth(3)},
                border: TableBorder.all(),
                children: <TableRow>[
                  TableRow(children: <TableCell>[
                    TableCell(
                      child: Column(
                        children: <Widget>[Image.asset('assets/images/move_logo.png'), Text('Move')],
                      ),
                    ),
                    TableCell(
                      child: Text('basic = G\nsur axe = G + X/Y/Z\nswitch axes = G + MMB\nfree move = G + G'),
                    ),
                    TableCell(
                      child: Image.asset('assets/images/move_ex.png'),
                    )
                  ]),
                  TableRow(children: <TableCell>[
                    TableCell(
                      child: Column(
                        children: <Widget>[Image.asset('assets/images/rotation_logo.png'), Text('Rotate')],
                      ),
                    ),
                    TableCell(
                      child: Text('basic =  R\nsur un axe =  R + X/Y/Z\nswitch entre les axes =  R + MMB\nfree rotate = R + R'),
                    ),
                    TableCell(
                      child: Image.asset('assets/images/rotation_ex.png'),
                    )
                  ]),
                  TableRow(children: <TableCell>[
                    TableCell(
                      child: Column(
                        children: <Widget>[Image.asset('assets/images/scale_logo.png'), Text('Scale')],
                      ),
                    ),
                    TableCell(
                      child: Text('basic = S\nsur un axe = S + X/Y/Z\nswitch entre les axes = S + MMB\nfree scale = S + S'),
                    ),
                    TableCell(
                      child: Image.asset('assets/images/scale_ex.png'),
                    )
                  ]),
                ]),
            Text("Pour faire l'action sur 2 axes \n   ex: Y/Z on fait Shift + X")
          ],
        ));
  }
}
