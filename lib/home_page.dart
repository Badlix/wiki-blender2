import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wiki_blender/shorcut_page.dart';
import '3D_viewportPage.dart';
import 'basic_3D_viewport.dart';

class HomePage extends StatelessWidget {
  static const routeName = 'home';

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        color: Colors.black,
        child: WikiWidget(),
        // child: FutureBuilder(
        //     future: _loadAsset('Shortcuts'),
        //     builder: (context, snapshot) {
        //       return WikiWidget(fichierJson: snapshot.data);
        //     }),
      ),
    );
  }

  // Future<String> _loadAsset(String fichierJson) {
  //   return rootBundle.loadString('assets/json_files/$fichierJson.json');
  // }
}

class WikiWidget extends StatelessWidget {
  WikiWidget({Key key, this.fichierJson}) : super(key: key);
  String fichierJson;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Wiki Blender"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.home),
            onPressed: () {},
          )
        ],
      ),
      body: ListView(
        children: <Widget>[_buildShortcuts(context), _buildLayout(context), _buildBasics(context), _buildMyTest(context)],
        padding: EdgeInsets.all(10),
        addAutomaticKeepAlives: true,
      ),
    );
  }
}

Widget _buildShortcuts(BuildContext context) {
  return Card(
      child: ListTile(
    title: Text('Shortcuts'),
    onTap: () {
      Navigator.pushNamed(context, ShortcutsPage.routeName);
    },
  ));
}

Widget _buildLayout(BuildContext context) {
  return SingleChildScrollView(
      child: Card(
          child: ExpansionTile(
    title: Text('Layout'),
    children: <Widget>[
      ListTile(
        title: Text(
          "3D view",
        ),
        onTap: () {
          Navigator.pushNamed(
            context,
            DviewportPage.routeName,
          );
        },
      ),
      Divider(thickness: 1, height: 5),
      ListTile(
        title: Text("Properties menu"),
      ),
      Divider(thickness: 1, height: 5),
      ListTile(
        title: Text("Scene menu"),
      ),
      Divider(thickness: 1, height: 5),
      ListTile(
        title: Text("Timeline"),
      ),
      Divider(thickness: 1, height: 5),
      ListTile(
        title: Text("Tool/Object menu"),
      ),
      Divider(thickness: 1, height: 5),
      ListTile(
        title: Text('The different parts of the layout interface'),
        onTap: () {},
      )
    ],
  )));
}

Widget _buildBasics(BuildContext context) {
  return SingleChildScrollView(
    child: Card(
      child: ExpansionTile(
        title: Text('Basics'),
        children: <Widget>[
          ListTile(
              title: Text('3D viewport'),
              onTap: () {
                Navigator.pushNamed(
                  context,
                  basicDviewportPage.routeName,
                );
              })
        ],
      ),
    ),
  );
}

Widget _buildMyTest(BuildContext context) {
  return SingleChildScrollView(
    child: Card(
      child: ExpansionTile(
        title: Text('Window System'),
        children: <Widget>[
          ListTile(
            title: Text('test1'),
            onTap: () {},
          )
        ],
      ),
    ),
  );
}
