import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:wiki_blender/basic_3D_viewport.dart';
import 'package:wiki_blender/modellingPage.dart';
import 'package:wiki_blender/shorcut_page.dart';
import 'package:wiki_blender/3D_viewportPage.dart';
import 'home_page.dart';

void main() {
  // Permet de lancer l'application en mode desktop sur Linux
  // See https://github.com/flutter/flutter/wiki/Desktop-shells#target-platform-override
  // BLE : pratique pour tester sans lancer d'émulateur
  debugDefaultTargetPlatformOverride = TargetPlatform.fuchsia;

  runApp(new WikiApp());
}

class WikiApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Wiki Blender',
      debugShowCheckedModeBanner: false,
      theme: _buildThemeData(),
      initialRoute: HomePage.routeName,
      routes: <String, WidgetBuilder>{
        HomePage.routeName: (BuildContext context) => HomePage(),
        ShortcutsPage.routeName: (BuildContext context) => ShortcutsPage(),
        basicDviewportPage.routeName: (BuildContext context) => basicDviewportPage(),
        DviewportPage.routeName: (BuildContext context) => DviewportPage(),
        ModellingPage.routeName: (BuildContext context) => ModellingPage()
      },
    );
  }

  ThemeData _buildThemeData() {
    return ThemeData(
        brightness: Brightness.dark,
        fontFamily: 'Roboto',
        pageTransitionsTheme: PageTransitionsTheme(builders: {
          TargetPlatform.iOS: CupertinoPageTransitionsBuilder(),
          TargetPlatform.android: CupertinoPageTransitionsBuilder(),
          TargetPlatform.fuchsia: CupertinoPageTransitionsBuilder()
        }));
  }
}
