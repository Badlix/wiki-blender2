import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'modellingPage.dart';

class DviewportPage extends StatelessWidget {
  static const routeName = '3D_viewport';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Wiki Blender"),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.home),
              onPressed: () {},
            )
          ],
        ),
        body: ListView(
          children: <Widget>[_buildModelling(context)],
          padding: EdgeInsets.all(10),
          addAutomaticKeepAlives: true,
        ));
  }
}

Widget _buildModelling(BuildContext context) {
  return SingleChildScrollView(
    child: Card(
        child: ListTile(
      title: Text('Modelling'),
      onTap: () {
        Navigator.pushNamed(context, ModellingPage.routeName);
      },
    )),
  );
}
