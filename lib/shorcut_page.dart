import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'home_page.dart';
import 'dart:convert';

class ShortcutsPage extends StatelessWidget {
  static const routeName = 'Shortcuts';
  String jsonFilename = 'assets/json_files/Shortcuts.json';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Shortcuts"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.home),
            onPressed: () {
              Navigator.of(context).pushNamedAndRemoveUntil(HomePage.routeName, (Route<dynamic> routeName) => false);
            },
          )
        ],
      ),
      body: FutureBuilder(
        future: _loadAsset(jsonFilename),
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
              return Container(
                color: Colors.red,
              );
            default:
              if (snapshot.hasError) {
                return Center(
                  child: Text('Erreur: ${snapshot.error}'),
                );
              }
          }
          return ListView(children: <Widget>[
            _buildGeneral(context, snapshot.data),
            _build3DViewport(context, snapshot.data),
            _buildEditMode(context, snapshot.data),
            _buildUVEditor(context, snapshot.data),
            _buildImageEditor(context, snapshot.data),
            _buildNodes(context, snapshot.data),
            _buildCompositor(context, snapshot.data),
            _buildSculting(context, snapshot.data),
            _buildAnimation(context, snapshot.data),
            _buildGraphEditor(context, snapshot.data),
            _buildRigging(context, snapshot.data),
          ]);
        },
      ),
    );
  }
}

Future<String> _loadAsset(String fichierJson) {
  return rootBundle.loadString(fichierJson);
}

Widget _buildGeneral(BuildContext context, String jsonContent) {
  Map jsonShortcuts = json.decode(jsonContent);
  return SingleChildScrollView(
      child: ExpansionTile(
    title: Text('General'),
    children: <Widget>[
      Padding(
        padding: const EdgeInsets.only(left: 20),
        child: ExpansionTile(
          title: Text('General'),
          children: <Widget>[
            Table(
                border: TableBorder.all(),
                children: List.generate(22, (row) {
                  return TableRow(
                      children: List.generate(2, (column) {
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        jsonShortcuts['General'][row][column],
                        textAlign: TextAlign.center,
                      ),
                    );
                  }));
                }))
          ],
        ),
      ),
      Padding(
        padding: const EdgeInsets.only(left: 20),
        child: ExpansionTile(
          title: Text('Selection'),
          children: <Widget>[
            Table(
                border: TableBorder.all(),
                children: List.generate(10, (row) {
                  return TableRow(
                      children: List.generate(2, (column) {
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        jsonShortcuts['General Selection'][row][column],
                        textAlign: TextAlign.center,
                      ),
                    );
                  }));
                }))
          ],
        ),
      ),
      Padding(
        padding: const EdgeInsets.only(left: 20),
        child: ExpansionTile(
          title: Text('Window'),
          children: <Widget>[
            Table(
              border: TableBorder.all(),
              children: List.generate(3, (row) {
                return TableRow(
                    children: List.generate(2, (column) {
                  return Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      jsonShortcuts['Window General'][row][column],
                      textAlign: TextAlign.center,
                    ),
                  );
                }));
              }),
            )
          ],
        ),
      ),
      ExpansionTile(
        title: Text("Change Window Type (Under Cursor)"),
        children: <Widget>[
          Table(
              border: TableBorder.all(),
              children: List.generate(11, (row) {
                return TableRow(
                    children: List.generate(2, (column) {
                  return Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      jsonShortcuts['Change Window Type (Under Cursor)'][row][column],
                      textAlign: TextAlign.center,
                    ),
                  );
                }));
              }))
        ],
      ),
      ExpansionTile(
        title: Text("Rendering"),
        children: <Widget>[
          Table(
              border: TableBorder.all(),
              children: List.generate(5, (row) {
                return TableRow(
                    children: List.generate(2, (column) {
                  return Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      jsonShortcuts['Rendering'][row][column],
                      textAlign: TextAlign.center,
                    ),
                  );
                }));
              }))
        ],
      ),
    ],
  ));
}

Widget _build3DViewport(BuildContext context, String jsonContent) {
  Map jsonShortcuts = json.decode(jsonContent);
  return SingleChildScrollView(
    child: ExpansionTile(
      title: Text("3D viewport"),
      children: <Widget>[
        ExpansionTile(
          title: Text('Navigation'),
          children: <Widget>[
            Table(
                border: TableBorder.all(),
                children: List.generate(4, (row) {
                  return TableRow(
                      children: List.generate(2, (column) {
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        jsonShortcuts['Navigation (3D viewport)'][row][column],
                        textAlign: TextAlign.center,
                      ),
                    );
                  }));
                })),
          ],
        ),
        ExpansionTile(
          title: Text('View'),
          children: <Widget>[
            Table(
              border: TableBorder.all(),
              children: List.generate(15, (row) {
                return TableRow(
                    children: List.generate(2, (column) {
                  return Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      jsonShortcuts['View (3D viewport)'][row][column],
                      textAlign: TextAlign.center,
                    ),
                  );
                }));
              }),
            )
          ],
        ),
        ExpansionTile(
          title: Text('Object Mode'),
          children: <Widget>[
            Table(
              border: TableBorder.all(),
              children: List.generate(18, (row) {
                return TableRow(
                    children: List.generate(2, (column) {
                  return Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      jsonShortcuts['Object Mode (3D viewport)'][row][column],
                      textAlign: TextAlign.center,
                    ),
                  );
                }));
              }),
            )
          ],
        ),
        ExpansionTile(
          title: Text('Shading'),
          children: <Widget>[
            Table(
              border: TableBorder.all(),
              children: List.generate(2, (row) {
                return TableRow(
                    children: List.generate(2, (column) {
                  return Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      jsonShortcuts['Shading (3D viewport)'][row][column],
                      textAlign: TextAlign.center,
                    ),
                  );
                }));
              }),
            )
          ],
        )
      ],
    ),
  );
}

Widget _buildEditMode(BuildContext context, String jsonContent) {
  Map jsonShortcuts = json.decode(jsonContent);
  return SingleChildScrollView(
    child: ExpansionTile(
      title: Text("Edit Mode"),
      children: <Widget>[
        ExpansionTile(
          title: Text('Selection'),
          children: <Widget>[
            Table(
                border: TableBorder.all(),
                children: List.generate(10, (row) {
                  return TableRow(
                      children: List.generate(2, (column) {
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        jsonShortcuts['Selection (Edit Mode)'][row][column],
                        textAlign: TextAlign.center,
                      ),
                    );
                  }));
                }))
          ],
        ),
        ExpansionTile(
          title: Text('Curve Editing'),
          children: <Widget>[
            Table(
                border: TableBorder.all(),
                children: List.generate(7, (row) {
                  return TableRow(
                      children: List.generate(2, (column) {
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        jsonShortcuts['Curve Editing (Edit Mode)'][row][column],
                        textAlign: TextAlign.center,
                      ),
                    );
                  }));
                }))
          ],
        ),
        ExpansionTile(
          title: Text('Modelling'),
          children: <Widget>[
            Table(
                border: TableBorder.all(),
                children: List.generate(19, (row) {
                  return TableRow(
                      children: List.generate(2, (column) {
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        jsonShortcuts['Modelling (Edit Mode)'][row][column],
                        textAlign: TextAlign.center,
                      ),
                    );
                  }));
                }))
          ],
        ),
        ExpansionTile(
          title: Text('Texturing'),
          children: <Widget>[
            Table(
                border: TableBorder.all(),
                children: List.generate(13, (row) {
                  return TableRow(
                      children: List.generate(2, (column) {
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        jsonShortcuts['View (3D viewport)'][row][column],
                        textAlign: TextAlign.center,
                      ),
                    );
                  }));
                }))
          ],
        )
      ],
    ),
  );
}

Widget _buildUVEditor(BuildContext context, String jsonContent) {
  Map jsonShortcuts = json.decode(jsonContent);
  return SingleChildScrollView(
    child: ExpansionTile(
      title: Text("UV Editor"),
      children: <Widget>[
        Table(
            border: TableBorder.all(),
            children: List.generate(6, (row) {
              return TableRow(
                  children: List.generate(2, (column) {
                return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    jsonShortcuts['UV Editor'][row][column],
                    textAlign: TextAlign.center,
                  ),
                );
              }));
            }))
      ],
    ),
  );
}

Widget _buildImageEditor(BuildContext context, String jsonContent) {
  Map jsonShortcuts = json.decode(jsonContent);
  return SingleChildScrollView(
    child: ExpansionTile(
      title: Text("Image Editor"),
      children: <Widget>[
        ExpansionTile(
          title: Text('Image Editor (View)'),
          children: <Widget>[
            Table(
                border: TableBorder.all(),
                children: List.generate(8, (row) {
                  return TableRow(
                      children: List.generate(2, (column) {
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        jsonShortcuts['Image Editor (View)'][row][column],
                        textAlign: TextAlign.center,
                      ),
                    );
                  }));
                }))
          ],
        ),
        ExpansionTile(
          title: Text('Image Editor (Paint)'),
          children: <Widget>[
            Table(
                border: TableBorder.all(),
                children: List.generate(7, (row) {
                  return TableRow(
                      children: List.generate(2, (column) {
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        jsonShortcuts['Image Editor (Paint)'][row][column],
                        textAlign: TextAlign.center,
                      ),
                    );
                  }));
                }))
          ],
        )
      ],
    ),
  );
}

Widget _buildNodes(BuildContext context, String jsonContent) {
  Map jsonShortcuts = json.decode(jsonContent);
  return SingleChildScrollView(
    child: ExpansionTile(
      title: Text("Nodes (Materials/Compositor)"),
      children: <Widget>[
        Table(
            border: TableBorder.all(),
            children: List.generate(11, (row) {
              return TableRow(
                  children: List.generate(2, (column) {
                return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    jsonShortcuts['Nodes (Materials/Compositor)'][row][column],
                    textAlign: TextAlign.center,
                  ),
                );
              }));
            }))
      ],
    ),
  );
}

Widget _buildCompositor(BuildContext context, String jsonContent) {
  Map jsonShortcuts = json.decode(jsonContent);
  return SingleChildScrollView(
    child: ExpansionTile(
      title: Text("Compositor"),
      children: <Widget>[
        Table(
            border: TableBorder.all(),
            children: List.generate(3, (row) {
              return TableRow(
                  children: List.generate(2, (column) {
                return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    jsonShortcuts['Compositor'][row][column],
                    textAlign: TextAlign.center,
                  ),
                );
              }));
            }))
      ],
    ),
  );
}

Widget _buildSculting(BuildContext context, String jsonContent) {
  Map jsonShortcuts = json.decode(jsonContent);
  return SingleChildScrollView(
    child: ExpansionTile(
      title: Text("Sculting"),
      children: <Widget>[
        Table(
            border: TableBorder.all(),
            children: List.generate(11, (row) {
              return TableRow(
                  children: List.generate(2, (column) {
                return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    jsonShortcuts['Sculting'][row][column],
                    textAlign: TextAlign.center,
                  ),
                );
              }));
            }))
      ],
    ),
  );
}

Widget _buildAnimation(BuildContext context, String jsonContent) {
  Map jsonShortcuts = json.decode(jsonContent);
  return SingleChildScrollView(
    child: ExpansionTile(
      title: Text("Animation"),
      children: <Widget>[
        ExpansionTile(
          title: Text('General'),
          children: <Widget>[
            Table(
                border: TableBorder.all(),
                children: List.generate(8, (row) {
                  return TableRow(
                      children: List.generate(2, (column) {
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        jsonShortcuts['Animation General'][row][column],
                        textAlign: TextAlign.center,
                      ),
                    );
                  }));
                }))
          ],
        ),
        ExpansionTile(
          title: Text('Timeline/Dopesheet/Graph Editor'),
          children: <Widget>[
            Table(
                border: TableBorder.all(),
                children: List.generate(15, (row) {
                  return TableRow(
                      children: List.generate(2, (column) {
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        jsonShortcuts['Animation (Timeline/Dopesheet/Graph Editor)'][row][column],
                        textAlign: TextAlign.center,
                      ),
                    );
                  }));
                }))
          ],
        )
      ],
    ),
  );
}

Widget _buildGraphEditor(BuildContext context, String jsonContent) {
  Map jsonShortcuts = json.decode(jsonContent);
  return SingleChildScrollView(
    child: ExpansionTile(
      title: Text("Graph Editor"),
      children: <Widget>[
        Table(
            border: TableBorder.all(),
            children: List.generate(3, (row) {
              return TableRow(
                  children: List.generate(2, (column) {
                return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    jsonShortcuts['Graph Editor'][row][column],
                    textAlign: TextAlign.center,
                  ),
                );
              }));
            }))
      ],
    ),
  );
}

Widget _buildRigging(BuildContext context, String jsonContent) {
  Map jsonShortcuts = json.decode(jsonContent);
  return SingleChildScrollView(
    child: ExpansionTile(
      title: Text("Rigging (Armatures)"),
      children: <Widget>[
        Table(
            border: TableBorder.all(),
            children: List.generate(13, (row) {
              return TableRow(
                  children: List.generate(2, (column) {
                return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    jsonShortcuts['Rigging (Armatures)'][row][column],
                    textAlign: TextAlign.center,
                  ),
                );
              }));
            }))
      ],
    ),
  );
}
