import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ModellingPage extends StatelessWidget {
  static const routeName = 'Modelling';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Wiki Blender"),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.home),
              onPressed: () {},
            )
          ],
        ),
        body: ListView(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Card(
                child: Text(
                    "Quand on ajoute une forme un petit menu apparait en bas a gauche de l'ecran, il sert a modifier les mesures de base de la forme\n(si on fait un clic autre que sur ce menu le menu disparait"),
              ),
            ),
            Column(
              children: <Widget>[Image.asset('assets/images/modelling_menu_plier')],
            ),
            Column(
              children: <Widget>[Image.asset('assets/images/modelling_menu_deplier')],
            )
          ],
        ));
  }
}
